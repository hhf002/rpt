package org.hhf.rpt.service;

import org.hhf.rpt.domain.I18n;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class I18nServiceImpl implements I18nService {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public I18n getMsg(String bundle, String locale, String code) {
        String sql = "SELECT * FROM T_I18N WHERE BUNDLE = ? AND LOCALE = ? AND CODE = ? AND STATE = 1";
        List<I18n> i18ns = jdbcTemplate.queryForList(sql, I18n.class, new Object[]{bundle, locale, code});
        if (i18ns != null && !i18ns.isEmpty()) {
            return i18ns.get(0);
        }
        return null;
    }

    @Override
    public I18n getDefaultMsg(String code) {
        return getMsg("base", "zh_CN", code);
    }

    @Override
    public I18n getZhMsg(String bundle, String code) {
        return getMsg(bundle, "zh_CN", code);
    }

    @Override
    public I18n getBaseMsg(String local, String code) {
        return getMsg("base", local, code);
    }

}
