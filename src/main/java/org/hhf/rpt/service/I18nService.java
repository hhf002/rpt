package org.hhf.rpt.service;

import org.hhf.rpt.domain.I18n;

public interface I18nService {
    I18n getMsg(String bundle, String locale, String code);
    I18n getDefaultMsg(String code);
    I18n getZhMsg(String bundle, String code);
    I18n getBaseMsg(String local, String code);
}
