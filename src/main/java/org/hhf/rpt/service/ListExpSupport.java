package org.hhf.rpt.service;

import lombok.extern.slf4j.Slf4j;
import org.hhf.rpt.domain.PageBean;
import org.hhf.rpt.domain.PageSupport;
import org.hhf.rpt.exception.CoreAssert;
import org.hhf.rpt.util.ExportUtils;
import org.hhf.rpt.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @author haohaifeng
 * @date 2021/1/22 11:34
 */
@Slf4j
@Component
public class ListExpSupport {
    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Autowired
    private PageSupport pageSupport;
    @Value("${rpt.xml.dir.absPath}")
    private String rptXmlDir;

    public void exp(HttpServletResponse response, Map<String, Object> queryArgs) throws IOException, SQLException {
        String rptCode = String.valueOf(queryArgs.get("rptCode"));
        CoreAssert.notBlank(rptCode, "rptCode为空");
        SqlParser sqlParser = new SqlParser(rptCode, rptXmlDir);
        String fileName = sqlParser.getRptName();
        if (StringUtils.isBlank(fileName)) {
            fileName = String.valueOf(System.nanoTime());
        }
        String sql = sqlParser.parseSQL(queryArgs);
        String[] titleCols = sqlParser.getTitleCols();
        DataSource dataSource = jdbcTemplate.getDataSource();
        Connection connection = dataSource.getConnection();
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(sql);
        List<Map<String, String>> list = convertList(resultSet);
        ExportUtils.exportExcel(response, fileName, list, titleCols);
    }

    public PageBean pageList(Map<String, Object> queryArgs) {
        String rptCode = String.valueOf(queryArgs.get("rptCode"));
        CoreAssert.notBlank(rptCode, "rptCode为空");
        Object pageSizeObj = queryArgs.get("pageSize");
        Object pageNoObj = queryArgs.get("pageNo");
        PageBean pageResult = null;
        SqlParser sqlParser = new SqlParser(rptCode, rptXmlDir);
        String sql = sqlParser.parseSQL(queryArgs);
        if (pageSizeObj == null || StringUtils.isBlank(pageSizeObj.toString())
                || pageNoObj == null || StringUtils.isBlank(pageNoObj.toString())) {
            pageResult = new PageBean();
            List<Map<String, Object>> maps = jdbcTemplate.queryForList(sql);
            pageResult.setList(maps);
            pageResult.setTotal(maps.size());
        } else {
            Integer pageSize = Integer.parseInt(pageSizeObj.toString());
            Integer pageNum = Integer.parseInt(pageNoObj.toString());
            PageBean pageBean = new PageBean();
            pageBean.setPageSize(pageSize);
            pageBean.setPageNo(pageNum);
            pageResult = pageSupport.queryByPage(sql, pageBean);
        }
        return pageResult;
    }

    private List<Map<String, String>> convertList(ResultSet rs) throws SQLException {
        List<Map<String, String>> list = new ArrayList();
        ResultSetMetaData md = rs.getMetaData();
        int columnCount = md.getColumnCount();
        while (rs.next()) {
            Map<String, String> rowData = new LinkedHashMap();
            for (int i = 1; i <= columnCount; i++) {
                rowData.put(md.getColumnName(i), rs.getString(i));
            }
            list.add(rowData);
        }
        return list;
    }
}