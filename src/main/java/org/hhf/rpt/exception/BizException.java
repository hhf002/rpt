package org.hhf.rpt.exception;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author haohaifeng
 * @date 2021/2/7 16:11
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class BizException extends RuntimeException {
    private String errorCode;
    private String errorMsg;
    // 国际化message参数
    private Object[] msgParams;

    public BizException() {
        super();
    }

    public BizException(String errorCode) {
        super(errorCode);
        this.errorCode = errorCode;
    }
    public BizException(String errorCode, Object[] msgParams) {
        super(errorCode);
        this.errorCode = errorCode;
        this.msgParams = msgParams;
    }

    public BizException(String errorCode, String errorMsg) {
        super(errorMsg);
        this.errorCode = errorCode;
        this.errorMsg = errorMsg;
    }

}