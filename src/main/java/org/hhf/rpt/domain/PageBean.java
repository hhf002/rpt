package org.hhf.rpt.domain;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class PageBean implements Serializable {
    private static final long serialVersionUID = 8656597559014685635L;
    /**
     * 总记录数
     */
    private long total;
    /**
     * 结果集
     */
    private List list;
    /**
     * 第几页
     */
    private Integer pageNo;
    /**
     * 每页记录数
     */
    private Integer pageSize;
    /**
     * 总页数
     */
    private Integer pages;
    /**
     * 当前页的数量 <= pageSize，该属性来自ArrayList的size属性
     */
    private Integer size;
}