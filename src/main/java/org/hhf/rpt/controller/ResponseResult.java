package org.hhf.rpt.controller;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class ResponseResult<T> {
	/** 业务错误码 */
	private String code;
	/** 信息描述 */
	private String message;
	/** 返回参数 */
	private T data;

	private ResponseResult(ResponseStatus resultStatus, T data) {
		this.code = resultStatus.getCode();
		this.message = resultStatus.getMessage();
		this.data = data;
	}
	private ResponseResult(String errorCode, String errorMsg) {
		this.code = errorCode;
		this.message = errorMsg;
	}

	public static ResponseResult<Void> success() {
		return new ResponseResult<Void>(ResponseStatus.SUCCESS, null);
	}

	public static <T> ResponseResult<T> success(T data) {
		return new ResponseResult<T>(ResponseStatus.SUCCESS, data);
	}

	public static <T> ResponseResult<T> success(ResponseStatus resultStatus, T data) {
		if (resultStatus == null) {
			return success(data);
		}
		return new ResponseResult<T>(resultStatus, data);
	}

	public static <T> ResponseResult<T> failure() {
		return new ResponseResult<T>(ResponseStatus.INTERNAL_SERVER_ERROR, null);
	}

	public static <T> ResponseResult<T> failure(String errorCode, String errorMsg) {
		return new ResponseResult<T>(errorCode, errorMsg);
	}

	public static <T> ResponseResult<T> failure(ResponseStatus resultStatus) {
		return failure(resultStatus, null);
	}

	public static <T> ResponseResult<T> failure(ResponseStatus resultStatus, T data) {
		if (resultStatus == null) {
			return new ResponseResult<T>(ResponseStatus.INTERNAL_SERVER_ERROR, null);
		}
		return new ResponseResult<T>(resultStatus, data);
	}
}