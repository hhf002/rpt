package org.hhf.rpt.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.hhf.rpt.domain.PageBean;
import org.hhf.rpt.service.ListExpSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Map;

/**
 * @author haohaifeng
 * @date 2021/1/22 15:46
 */
@Api(tags="简单报表开发工具")
@RestController
@RequestMapping(value = "/rpt")
public class RptController {

    @Autowired
    private ListExpSupport listExpSupport;

    @ApiOperation("查询接口")
    @PostMapping("/601/list")
    public ResponseResult<PageBean> list(@RequestBody Map<String, Object> queryArgs) {
        PageBean data = listExpSupport.pageList(queryArgs);
        return ResponseResult.success(data);
    }
    @ApiOperation("导出EXCEL接口")
    @PostMapping("/602/exp")
    public void exp(HttpServletResponse response, @RequestBody Map<String, Object> queryArgs) throws IOException, SQLException {
        listExpSupport.exp(response, queryArgs);
    }

    @ApiOperation("GET方式查询接口")
    @GetMapping("/600/list")
    public ResponseResult<PageBean> getList(@RequestParam Map<String, Object> queryArgs) {
        PageBean data = listExpSupport.pageList(queryArgs);
        return ResponseResult.success(data);
    }
    @ApiOperation("GET方式导出EXCEL接口")
    @GetMapping("/600/exp")
    public void doExp(HttpServletResponse response, @RequestParam Map<String, Object> queryArgs) throws IOException, SQLException {
        listExpSupport.exp(response, queryArgs);
    }
}
