-- 页面异常提示i18n支持
create table t_i18n(
   id bigint NOT NULL AUTO_INCREMENT,
   bundle varchar(50),
   locale varchar(20),
   code varchar(100),
   msg varchar(300),
   sort int,
   state tinyint,
   PRIMARY KEY (`id`) USING BTREE
);

INSERT INTO t_i18n VALUES ('base','zh_CN','100100','XML报表文件读取异常','1','1');
INSERT INTO t_i18n VALUES ('base','zh_CN','100101','找不到XML报表文件“{0}”，请联系管理员','1','1');