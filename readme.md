## 通用简单报表配置工具

### 1 一句话描述
通过在XML中配置SQL信息，经统一的Rest接口传参报表编码rptCode，提供报表JSON数据服务和Excel数据导出服务。
**多数据源版本见：https://gitee.com/hhf002/ds_rpt.git 支持报表在线编辑，可免去服务器上修改配置的麻烦**

### 2 特点： 
+ 适用于前后端分离的项目后端集成或单独部署报表服务，仅提供后端Rest接口，足够干净
+ XML编写SQL获取报表所需数据，统一到一个Rest接口获取数据，维护成本低。
+ 配置一份XML报表文件即可同时支持浏览器端数据EXCEL导出，所见即所得
+ SQL即时解析，修改XML后报表即时生效，无需重启服务
+ XML文件结构简单一目了然，查询条件自动判空，SQL写法和在SQL客户端写法非常相似，SQL整洁干净
+ 支持异常提示国际化配置，数据库配置无需重启服务，即时生效

### 3 环境要求：
IDEA，JDK8，MySQL/MariaDB(配置文件的数据库连接配置为MariaDB_v10.3.13的)

### 4 启动
 1. application-*.yml配置MySQL数据库连接，修改XML文件存放的外部路径rpt.xml.dir.absPath。如果未配置该路径，则默认访问本包rpt目录下的xml文件
 2. 执行数据库脚本：resources/db/delta.sql，初始化国际化表，该表配置后可以按错误编码覆盖代码中部分提示语句，方便修改提示内容。
 3. 执行Application中的main方法启动springboot

### 5 Swagger地址
http://[ip]:7318/swagger-ui.html


### 6 访问通用报表接口
```
通用报表接口传参参考

POST http://localhost:7318/rpt/601/list
Content-Type: application/json

{
  "rptCode":"RPT_I18N",
  "pageSize":"",
  "pageNo":"",
  "msg":"异常"
}
###
```

### 7 访问通用Excel导出接口
```
通用Excel导出接口传参参考
POST http://localhost:7318/rpt/602/exp
Content-Type: application/json

{
  "rptCode":"RPT_I18N",
  "msg":"异常"
}
###
``` 

### 8 参数简介
+ rptCode：报表编码，一张报表有唯一的一个编码，也是xml文件根标签名称。
+ pageSize：分页参数，每页展示条数，不传不分页
+ pageNo：分页参数，展示第几页，不传不分页
+ msg：自定义的业务SQL查询字段

### 9 XML结构介绍
```
<?xml version="1.0" encoding="UTF-8"?>
<!-- show="true" 表示日志打印会输出最终数据库执行sql-->
<sqls show="true">
<!-- 
RPT_I18N标签： RPT_I18N为rptCode，必需和xml的文件名保持一志，也就是说一张报表对应一个XML文件
-->
<RPT_I18N>
   <!-- rptName标签： 标签值为报表名称、导出excel的文件名称-->
   <rptName>自定义问卷列表</rptName>
   <!-- colNames标签： 报表导出excel的列头，字段名用英文逗号隔开-->
   <colNames>ID,资源包,语言,键,值,排序,是否有效</colNames>
   <!-- 
   sql标签： SQL正文，放在<![CDATA[]]>标签中可排除部分xml特殊字符解析的影响。
   where 后的不确定查询条件可以放置在一对{}内，可避免写诸如if else的判断。
   查询参数表达式#{#msg}为spel表达式写法。该参数表达式支持部分spel表达式功能。
   -->
   <sql>
      <![CDATA[
        SELECT
          id,
          bundle,
          locale,
          `code`,
          msg,
          sort,
          state
        FROM t_i18n
        where 1=1
        { and msg like concat('%',#{#msg},'%')}
      ]]>
   </sql>
</RPT_I18N>
</sqls>
```
